﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    /// <summary>
    /// The target model's profile
    /// </summary>
    internal class TargetModel
    {
        public TargetModel()
        {
            ArmourSave = 3;
            InvulSave = 7;
            FeelNoPainSave = 7;
            Toughness = 4;
            Wounds = 1;
            IsInLightCover = false;
            IsVehicle = false;
            Range = 12;
        }

        /// <summary>
        /// The target's armour save
        /// </summary>
        public int ArmourSave { get; set; }

        /// <summary>
        /// The target's invulnerable save
        /// </summary>
        public int InvulSave { get; set; }

        /// <summary>
        /// The target's Feel No Pain save
        /// </summary>
        public int FeelNoPainSave { get; set; }

        /// <summary>
        /// The target's toughness
        /// </summary>
        public int Toughness { get; set; }

        /// <summary>
        /// The target's wounds
        /// </summary>
        public int Wounds { get; set; }

        /// <summary>
        /// Is the target in Light Cover?
        /// 
        /// This gives +1 to the saving throw in 40k
        /// 
        /// Used as "Obscured" for Kill Team
        /// </summary>
        public bool IsInLightCover { get; set; }

        /// <summary>
        /// Is the target in Dense Cover?
        /// 
        /// This gives -1 to the attack roll in 40k
        /// 
        /// Ignored for Kill Team; use <see cref="IsInLightCover"/> instead
        /// </summary>
        public bool IsInDenseCover { get; set; }

        /// <summary>
        /// Is the target a vehicle?
        /// </summary>
        public bool IsVehicle { get; set; }

        /// <summary>
        /// How far away is the target model?
        /// </summary>
        public int Range { get; set; }

        /// <summary>
        /// Does the target get to reroll its saves?
        /// </summary>
        public Reroll SaveReroll { get; set; }
    }
}
