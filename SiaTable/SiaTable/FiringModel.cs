﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    /// <summary>
    /// The model that's firing the weapon
    /// </summary>
    internal class FiringModel
    {
        public FiringModel()
        {
            BS = 3;

            WH40kBackend = new WH40kSiaCalculator();
            KillTeamBackend = new KillTeamSiaCalculator();
        }

        private KillTeamSiaCalculator KillTeamBackend { get; set; }
        private WH40kSiaCalculator WH40kBackend { get; set; }

        /// <summary>
        /// The model's ballistic skill
        /// </summary>
        public int BS { get; set; }

        /// <summary>
        /// Does this model get a reroll on its shooting rolls?
        /// </summary>
        public Reroll HitReroll
        {
            get; set;
        }

        /// <summary>
        /// Does this model get a reroll on its to-wound rolls?
        /// </summary>
        public Reroll WoundReroll
        {
            get; set;
        }

        /// <summary>
        /// If true, we use the Kill Team hit modifiers
        /// </summary>
        public bool UseKillTeamRules { get; set; }

        /// <summary>
        /// Did the firing model move this turn?
        /// </summary>
        public bool Moved { get; set; }

        /// <summary>
        /// Did the firing model advance this turn?
        /// </summary>
        public bool Advanced { get; set; }

        /// <summary>
        /// Is the firing model a Terminator, Bike, or otherwise benefitting from the Beta Bolter rules?
        /// </summary>
        public bool IsTerminatorOrBike { get; set; }

        public double ExpectedWoundsVs(TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            if (UseKillTeamRules)
                return KillTeamBackend.CalculateExpectedWounds(this, target, gun, ammo);
            else
                return WH40kBackend.CalculateExpectedWounds(this, target, gun, ammo);
        }
    }
}
