﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    /// <summary>
    /// A weapon profile we use to calculate the odds of hitting/wounding
    /// </summary>
    internal class Weapon
    {
        /// <summary>
        /// Create a weapon with the default (Boltgun) profile
        /// </summary>
        public Weapon()
        {
            Strength = 4;
            AP = 0;
            Range = 24;
            Type = WeaponType.RapidFire;
            NumShots = 1;
            Damage = 1;

            GetsSpecialAmmo40k = true;
            GetsSpecialAmmoKillTeam = true;
        }

        /// <summary>
        /// What row of the table is this weapon on?
        /// </summary>
        public int TableRow { get; set; }

        /// <summary>
        /// The name of this weapon
        /// 
        /// Not used anywhere, but useful for debugging
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The strength of this weapon's base profile
        /// </summary>
        public int Strength { get; set; }

        /// <summary>
        /// This weapon's base AP modifier (always 0 or negative)
        /// </summary>
        public int AP { get; set; }

        /// <summary>
        /// The weapon's base AP in Kill Team (always 0 or negative)
        /// </summary>
        public int? KillTeamAP { get; set; }

        /// <summary>
        /// The range of this weapon's profile (in inches)
        /// </summary>
        public int Range { get; set; }

        /// <summary>
        /// Additional to-hit modifier that is intrinsic to the weapon
        /// 
        /// e.g. -1 to hit when firing both parts of a combi-weapon
        /// </summary>
        public int ToHitMod { get; set; }

        public enum WeaponType
        {
            RapidFire,
            Assault,
            Heavy,
            Pistol
        }

        /// <summary>
        /// The type of weapon
        /// 
        /// Rapid Fire weapons double their shots at close range
        /// </summary>
        public WeaponType Type { get; set; }

        /// <summary>
        /// The number of shots this weapon fires
        /// 
        /// e.g. the 1 in Rapid Fire 1
        /// </summary>
        public int NumShots { get; set; }

        /// <summary>
        ///  The number of shots this weapon gets in Kill Team
        /// </summary>
        public int? KillTeamNumShots { get; set; }

        /// <summary>
        /// The weapon's damage characteristic.  This does NOT take into account special abilities e.g. Roll of 6 = Moral Wound
        /// 
        /// For randomized damage, use <see cref="DAMAGE_2D6"/>, <see cref="DAMAGE_D3"/> or <see cref="DAMAGE_D6"/> as appropriate
        /// </summary>
        public int Damage { get; set; }

        /// <summary>
        /// The amount of damage this weapon deals in Kill Team
        /// </summary>
        public int? KillTeamDamage { get; set; }

        public const int DAMAGE_D6 = -1;
        public const int DAMAGE_D3 = -2;
        public const int DAMAGE_2D6 = -3;

        /// <summary>
        /// Does this weapon get SIA in 40k 9e?
        /// </summary>
        public bool GetsSpecialAmmo40k { get; set; }

        /// <summary>
        /// Does this weapon get SIA in Kill Team?
        /// </summary>
        public bool GetsSpecialAmmoKillTeam { get; set; }
    }
}
