﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    internal abstract class SiaCalculator
    {
        /// <summary>
        /// Calculate the expected number of wounds inflicted by the shooter firing the given gun with the given ammo at the target
        /// </summary>
        /// <param name="shooter"></param>
        /// <param name="target"></param>
        /// <param name="gun"></param>
        /// <param name="ammo"></param>
        /// <returns></returns>
        public abstract double CalculateExpectedWounds(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo);

        /// <summary>
        /// Calculate the required die roll to HIT the target
        /// </summary>
        /// <param name="shooter"></param>
        /// <param name="target"></param>
        /// <param name="gun"></param>
        /// <param name="ammo"></param>
        /// <returns></returns>
        protected abstract int ToHitVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo);

        /// <summary>
        /// Calculate the required die roll to WOUND the target
        /// </summary>
        /// <param name="shooter"></param>
        /// <param name="target"></param>
        /// <param name="gun"></param>
        /// <param name="ammo"></param>
        /// <returns></returns>
        protected abstract int ToWoundVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo);

        /// <summary>
        /// Calculate the required die roll to PASS the target's save (best between invul and armour)
        /// </summary>
        /// <param name="shooter"></param>
        /// <param name="target"></param>
        /// <param name="gun"></param>
        /// <param name="ammo"></param>
        /// <returns></returns>
        protected abstract int ToSaveVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo);

        /// <summary>
        /// Calculate the number of shots the shooter makes vs the target
        /// </summary>
        /// <param name="shooter"></param>
        /// <param name="target"></param>
        /// <param name="gun"></param>
        /// <param name="ammo"></param>
        /// <returns></returns>
        protected abstract int NumShotsVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo);

        /// <summary>
        /// Calulate the odds of passing a die roll, taking rerolls into account
        /// </summary>
        /// <param name="resultNeeded">Must be in the range [2, 6], otherwise undefined results can happen</param>
        /// <param name="reroll"></param>
        /// <returns></returns>
        protected virtual double OddsToPassRoll(int resultNeeded, Reroll reroll)
        {
            double x;

            if (resultNeeded > 6)
                x = 0;
            else
                x = (7.0 - resultNeeded) / 6.0;


            switch (reroll)
            {
                case Reroll.RerollOnes:
                    // x odds of just outright passing, plus an additional x/6 for the reroll
                    x = x + 1.0 / 6.0 * x;
                    break;

                case Reroll.RerollFailures:
                    // y = 1.0-x will fail, so reroll all those and multiply by x
                    x = x + (1.0 - x) * x;
                    break;

                case Reroll.RerollSuccess:
                    // flip to odds of failing and calculate as above
                    x = 1.0 - x;
                    x = x + (1.0 - x) * x;
                    x = 1.0 - x;
                    break;

                case Reroll.None:
                default:
                    // no further calculations needed
                    break;

            }
            return x;
        }

        /// <summary>
        /// Get the multiplier for the amount of actual damage caused
        /// 
        /// Currently 40k and Kill Team don't allow overkill, so on a single-wound model we ignore the damage stat of the weapon
        /// and just apply a flat 1.0 multiplier
        /// 
        /// This may need to be overridden in a future rules revision
        /// </summary>
        /// <param name="target"></param>
        /// <param name="damageStat"></param>
        /// <returns></returns>
        protected abstract double DamageMultiplier(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo);

        /// <summary>
        /// The normal to-wound chart comparing strength vs toughness
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="toughness"></param>
        /// <returns></returns>
        protected int ToWoundVs(int strength, int toughness)
        {
            if (strength >= toughness * 2)
                return 2;
            else if (strength <= toughness / 2)
                return 6;
            else if (strength > toughness)
                return 3;
            else if (strength < toughness)
                return 5;
            else
                return 4;
        }
    }
}
