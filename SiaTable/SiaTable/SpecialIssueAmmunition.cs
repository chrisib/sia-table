﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    /// <summary>
    /// The different types of boltgun ammunition
    /// </summary>
    internal enum SpecialIssueAmmunition
    {
        /// <summary>
        /// No special rules; use the gun's normal profile
        /// </summary>
        None,

        /// <summary>
        /// 40k: Ignores the effects of cover
        /// 
        /// Kill Team: +1 to hit vs models in cover
        /// </summary>
        Dragonfire,

        /// <summary>
        /// 40k: +1 to wound vs non-vehicle & non-titanic targets
        /// 
        /// Kill Team: Always wounds on 2+ vs non-vehicles
        /// </summary>
        Hellfire,

        /// <summary>
        /// 40k: +6" range, -1 AP
        /// 
        /// Kill Team: +3" for pistols, +6" for all others, -1 AP to a minimum of -2
        /// </summary>
        Kraken,

        /// <summary>
        /// 40k: +1 damage
        /// 
        /// Kill Team: -3" for pistols, -6" for all others, -3 AP to a minimum of -3
        /// </summary>
        Vengeance
    }
}
