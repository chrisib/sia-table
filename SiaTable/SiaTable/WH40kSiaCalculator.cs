﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    internal class WH40kSiaCalculator : SiaCalculator
    {
        public override double CalculateExpectedWounds(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            if (!gun.GetsSpecialAmmo40k && ammo != SpecialIssueAmmunition.None)
                return 0.0;

            int numShots = NumShotsVs(shooter, target, gun, ammo);
            int toHit = ToHitVs(shooter, target, gun, ammo);
            int toWound = ToWoundVs(shooter, target, gun, ammo);
            int toSave = ToSaveVs(shooter, target, gun, ammo);
            int postSave = target.FeelNoPainSave;

            double oddsToHit = OddsToPassRoll(toHit, shooter.HitReroll);
            double oddsToWound = OddsToPassRoll(toWound, shooter.WoundReroll);
            double oddsToFailSave = 1.0 - OddsToPassRoll(toSave, target.SaveReroll);
            double oddsToFailPostSave = 1.0 - OddsToPassRoll(postSave, Reroll.None);
            double damageMultiplier = DamageMultiplier(shooter, target, gun, ammo);

            return numShots * oddsToHit * oddsToWound * oddsToFailSave * oddsToFailPostSave * damageMultiplier;
        }

        protected override double DamageMultiplier(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            double multiplier;
            if (target.Wounds == 1)
                return 1.0;
            else if (gun.Damage == Weapon.DAMAGE_2D6)
                multiplier = 7.0;
            else if (gun.Damage == Weapon.DAMAGE_D3)
                multiplier = 1.5;
            else if (gun.Damage == Weapon.DAMAGE_D6)
                multiplier = 3.5;
            else
                multiplier = gun.Damage;

            // 40k 9E Vengeance Ammo increases damage by 1
            if (ammo == SpecialIssueAmmunition.Vengeance)
                multiplier = multiplier + 1;

            return multiplier;
        }

        protected override int NumShotsVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            int maxRange = gun.Range;
            int shotMultiplier = 1;

            switch (ammo)
            {
                case SpecialIssueAmmunition.Kraken:
                    if (gun.Type == Weapon.WeaponType.Pistol)
                        maxRange = maxRange + 3;
                    else
                        maxRange = maxRange + 6;
                    break;

                case SpecialIssueAmmunition.Vengeance:
                    if (gun.Type == Weapon.WeaponType.Pistol)
                        maxRange = maxRange - 3;
                    else
                        maxRange = maxRange - 6;
                    break;
            }

            switch (gun.Type)
            {
                case Weapon.WeaponType.Pistol:
                    if (target.Range > maxRange)     // pistols can fire while within 1"
                        shotMultiplier = 0;
                    else if (shooter.Advanced)       // cannot fire pistols after advancing
                        shotMultiplier = 0;
                    else
                        shotMultiplier = 1;
                    break;

                case Weapon.WeaponType.RapidFire:
                    if (shooter.Advanced)                                     // cannot fire rapid fire weapons when advancing
                        shotMultiplier = 0;
                    else if (target.Range > maxRange || target.Range <= 1)    // cannot fire in close combat!
                        shotMultiplier = 0;
                    else if (target.Range <= maxRange / 2)                    // double shots within half range!
                        shotMultiplier = 2;
                    else if ((shooter.IsTerminatorOrBike || !shooter.Moved))  // Bolter Discipline allows models to gain extra shots if they stood still OR are bikes/terminators
                        shotMultiplier = 2;
                    else
                        shotMultiplier = 1;
                    break;

                case Weapon.WeaponType.Heavy:
                    if (shooter.Advanced)                                   // cannot fire heavy weapons after advancing
                        shotMultiplier = 0;
                    else if (target.Range > maxRange || target.Range <= 1)  // cannot fire in close combat!
                        shotMultiplier = 0;
                    else
                        return gun.NumShots;
                    break;

                default:
                    if (target.Range > maxRange || target.Range <= 1)     // cannot fire in close combat!
                        shotMultiplier = 0;
                    else
                        shotMultiplier = 1;
                    break;
            }

            return gun.NumShots * shotMultiplier;
        }

        protected override int ToHitVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            int mods = gun.ToHitMod;

            // Dense Cover gives -1 to hit, but Dragonfire Bolts ignore that!
            if (target.IsInDenseCover && ammo != SpecialIssueAmmunition.Dragonfire)
                mods = mods - 1;

            // -1 to hit when firing heavies on the move OR assault while advancing
            if ((shooter.Advanced && gun.Type == Weapon.WeaponType.Assault) || (shooter.Moved && gun.Type == Weapon.WeaponType.Heavy))
                mods = mods - 1;

            // 40k 9E clamps to-hit modifiers to the [-1, 1] range
            if (mods < -1)
                mods = -1;
            else if (mods > 1)
                mods = 1;

            int minDice = shooter.BS - mods; // subtract the modifer from the BS to get the minimum to-hit roll

            return Math.Min(6, minDice);
        }

        protected override int ToSaveVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            int ap = gun.AP;

            if (ammo == SpecialIssueAmmunition.Kraken)  // Kraken Rounds give -1 ap
                ap = ap - 1;

            if (target.IsInLightCover && ammo != SpecialIssueAmmunition.Dragonfire) // Dragonfire ignores light cover, otherwise improve the save by +1
                ap = ap + 1;

            if (target.ArmourSave - ap > 6)
                return target.InvulSave;
            else
                return Math.Min(target.InvulSave, target.ArmourSave - ap);
        }

        protected override int ToWoundVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            // Hellfire rounds get +1 to wound
            if (!target.IsVehicle && ammo == SpecialIssueAmmunition.Hellfire)
            {
                int dice = ToWoundVs(gun.Strength, target.Toughness);
                dice = Math.Min(dice - 1, 2);                           // +1 to hit <--> -1 to dice roll, min 2+
                return dice;
            }
            else
            {
                return ToWoundVs(gun.Strength, target.Toughness);
            }
        }
    }
}
