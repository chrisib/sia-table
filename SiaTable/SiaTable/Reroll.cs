﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    /// <summary>
    /// Different kinds of reroll that can be applied to a roll
    /// </summary>
    internal enum Reroll
    {
        /// <summary>
        /// No reroll allowed
        /// </summary>
        None,

        /// <summary>
        /// Reroll 1s
        /// </summary>
        RerollOnes,

        /// <summary>
        /// Reroll all failed rolls
        /// </summary>
        RerollFailures,

        /// <summary>
        /// Reroll all successful rolls
        /// </summary>
        RerollSuccess
    }
}
