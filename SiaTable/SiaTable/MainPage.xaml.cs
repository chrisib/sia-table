﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SiaTable
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            Target = new TargetModel();
            Shooter = new FiringModel();

            Guns = new List<Weapon>()
            {
                // standard Veteran weapon types
                new Weapon()
                {
                    Name="Boltgun",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.RapidFire,
                    NumShots=1,
                    AP=0,

                    TableRow=2
                },
                new Weapon()
                {
                    Name="Bolt Pistol",
                    Range=12,
                    Strength=4,
                    Type=Weapon.WeaponType.Pistol,
                    NumShots=1,
                    AP=0,

                    TableRow=3
                },
                new Weapon()
                {
                    Name="Stalker Boltgun",
                    Range=30,
                    Strength=4,
                    Type=Weapon.WeaponType.Heavy,

                    NumShots=1,
                    AP=-2,
                    Damage=2,

                    KillTeamNumShots=2,
                    KillTeamAP=-1,
                    KillTeamDamage=1,

                    TableRow=4
                },
                new Weapon()
                {
                    Name="Stormbolter",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.RapidFire,
                    NumShots=2,
                    AP=0,
                    GetsSpecialAmmo40k = false,

                    TableRow=5
                },
                new Weapon()
                {
                    Name="Combi-*",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.RapidFire,
                    NumShots=1,
                    AP=0,
                    ToHitMod=-1,

                    TableRow=6
                },

                // Intercessor weapon types
                new Weapon()
                {
                    Name="Bolt Rifle",
                    Range=30,
                    Strength=4,
                    Type=Weapon.WeaponType.RapidFire,
                    NumShots=1,
                    AP=-1,
                    GetsSpecialAmmo40k = false,

                    TableRow=8
                },
                new Weapon()
                {
                    Name="Stalker Bolt Rifle",
                    Range=36,
                    Strength=4,
                    Type=Weapon.WeaponType.Heavy,
                    NumShots=1,
                    AP=-2,
                    Damage = 2,
                    KillTeamDamage = 1,
                    GetsSpecialAmmo40k = false,

                    TableRow=9
                },
                new Weapon()
                {
                    Name="Auto Bolt Rifle",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.Assault,
                    NumShots=3,
                    KillTeamNumShots=2,
                    AP=0,
                    GetsSpecialAmmo40k = false,

                    TableRow=10
                },
                new Weapon()
                {
                    Name="Heavy Bolt Pistol",
                    Range=12,
                    Strength=4,
                    Type=Weapon.WeaponType.Pistol,
                    NumShots=1,
                    AP=-1,
                    GetsSpecialAmmo40k = false,

                    TableRow=11
                },

                // Reiver weapon types
                new Weapon()
                {
                    Name="Bolt Carbine",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.Assault,
                    NumShots=2,
                    AP=0,
                    GetsSpecialAmmo40k = false,

                    TableRow=13
                },
                new Weapon()
                {
                    Name="Heavy Bolt Pistol",
                    Range=12,
                    Strength=4,
                    Type=Weapon.WeaponType.Pistol,
                    NumShots=1,
                    AP=-1,
                    GetsSpecialAmmo40k = false,

                    TableRow=14
                },
                new Weapon()
                {
                    Name="Special Issue Bolt Pistol",
                    Range=12,
                    Strength=4,
                    Type=Weapon.WeaponType.Pistol,
                    NumShots=1,
                    AP=-2,
                    GetsSpecialAmmo40k = false,
                    GetsSpecialAmmoKillTeam = false,

                    TableRow=14
                },

                // Special HQ/Elite specific weapons
                new Weapon()
                {
                    Name="Vigil Spear",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.RapidFire,
                    NumShots=1,
                    AP=0,

                    TableRow=16
                },
                new Weapon()
                {
                    Name="Mastercrafted Boltgun",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.RapidFire,
                    NumShots=1,
                    AP=-1,
                    GetsSpecialAmmo40k = false,

                    TableRow=17
                },
                new Weapon()
                {
                    Name="Mastercrafted Auto Bolt Rifle",
                    Range=24,
                    Strength=4,
                    Type=Weapon.WeaponType.Assault,
                    NumShots=2,
                    AP=0,
                    GetsSpecialAmmo40k = false,

                    TableRow=18
                },
                new Weapon()
                {
                    Name="Mastercrafted Stalker Bolt Rifle",
                    Range=36,
                    Strength=4,
                    Type=Weapon.WeaponType.Heavy,
                    NumShots=1,
                    AP=-2,
                    GetsSpecialAmmo40k = false,

                    TableRow=19
                },
                new Weapon()
                {
                    Name="Absolver Bolt Pistol",
                    Range=16,
                    Strength=5,
                    Type=Weapon.WeaponType.Pistol,
                    NumShots=1,
                    AP=-1,
                    GetsSpecialAmmo40k = false,

                    TableRow=20
                }
            };
            

            InitializeComponent();

            foreach(Weapon w in Guns)
            {
                foreach (SpecialIssueAmmunition ammo in Enum.GetValues(typeof(SpecialIssueAmmunition)))
                {
                    Label l = new Label();
                    l.Style = Resources["styDefault"] as Style;
                    WeaponGrid.Children.Add(l);
                    Grid.SetRow(l, w.TableRow);
                    Grid.SetColumn(l, 1+(int)ammo);

                    if (ammo != SpecialIssueAmmunition.None)
                    {
                        if (!w.GetsSpecialAmmo40k)
                        {
                            Binding b = new Binding("IsToggled");
                            b.Source = chkKillTeam;
                            l.SetBinding(IsVisibleProperty, b);
                        }
                        else if (!w.GetsSpecialAmmoKillTeam)
                        {
                            Binding b = new Binding("IsToggled");
                            b.Source = chkKillTeam;
                            b.Converter = new NegateBooleanConverter();
                            l.SetBinding(IsVisibleProperty, b);
                        }
                    }
                }
            }

            cboHitReroll.SelectedIndex = 0;
            cboSvReroll.SelectedIndex = 0;
            cboWoundReroll.SelectedIndex = 0;

            RecalculateTable();
        }

        private TargetModel Target { get; set; }
        private FiringModel Shooter { get; set; }
        private List<Weapon> Guns { get; set; }

        private void RecalculateTable()
        {
            foreach (Weapon w in Guns)
            {
                double bestValueInRow = int.MinValue;
                Label bestInRow = null;

                foreach(SpecialIssueAmmunition ammo in Enum.GetValues(typeof(SpecialIssueAmmunition)))
                {
                    int i = 1 + (int)ammo;
                    IEnumerable<object> candiates = WeaponGrid.Children.Where(c => Grid.GetRow(c) == w.TableRow && Grid.GetColumn(c) == i) as IEnumerable<object>;
                    Label l = candiates.FirstOrDefault() as Label;

                    double wounds = Shooter.ExpectedWoundsVs(Target, w, (SpecialIssueAmmunition)(i - 1));

                    if (wounds > bestValueInRow)
                    {
                        bestValueInRow = wounds;
                        bestInRow = l;
                    }
                    l.Text = string.Format("{0:0.00}", wounds);
                }

                foreach (SpecialIssueAmmunition ammo in Enum.GetValues(typeof(SpecialIssueAmmunition)))
                {
                    int i = 1 + (int)ammo;
                    IEnumerable<object> candiates = WeaponGrid.Children.Where(c => Grid.GetRow(c) == w.TableRow && Grid.GetColumn(c) == i) as IEnumerable<object>;
                    Label l = candiates.FirstOrDefault() as Label;

                    if (bestValueInRow == 0.0)
                    {
                        l.BackgroundColor = Color.DarkGray;
                    }
                    else if (l == bestInRow)
                    {
                        l.BackgroundColor = Color.Green;
                    }
                    else
                    {
                        l.BackgroundColor = Color.Transparent;
                    }
                }
            }
        }

        private void NumRange_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double v = Math.Round(e.NewValue);
            numRange.Value = v;

            Target.Range = (int)v;
            RecalculateTable();
        }

        private void NumBS_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Shooter.BS = (int)e.NewValue;
            RecalculateTable();
        }

        private void NumW_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double v = Math.Round(e.NewValue);
            numW.Value = v;

            Target.Wounds = (int)v;
            RecalculateTable();
        }

        private void NumT_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double v = Math.Round(e.NewValue);
            numT.Value = v;

            Target.Toughness = (int)v;
            RecalculateTable();
        }

        private void NumSv_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Target.ArmourSave = (int)e.NewValue;
            RecalculateTable();
        }

        private void NumInv_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Target.InvulSave = (int)e.NewValue;
            RecalculateTable();
        }

        private void NumFnP_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Target.FeelNoPainSave = (int)e.NewValue;
            RecalculateTable();
        }

        private void ChkInLightCover_Toggled(object sender, ToggledEventArgs e)
        {
            Target.IsInLightCover = e.Value;
            RecalculateTable();
        }

        private void ChkInDenseCover_Toggled(object sender, ToggledEventArgs e)
        {
            Target.IsInDenseCover = e.Value;
            RecalculateTable();
        }

        private void ChkIsVehicle_Toggled(object sender, ToggledEventArgs e)
        {
            Target.IsVehicle = e.Value;
            RecalculateTable();
        }

        private void ChkKillTeam_Toggled(object sender, ToggledEventArgs e)
        {
            Shooter.UseKillTeamRules = e.Value;
            RecalculateTable();
        }

        private void CboHitReroll_SelectedIndexChanged(object sender, EventArgs e)
        {
            Shooter.HitReroll = (Reroll)cboHitReroll.SelectedIndex;
            RecalculateTable();
        }

        private void CboWoundReroll_SelectedIndexChanged(object sender, EventArgs e)
        {
            Shooter.WoundReroll = (Reroll)cboWoundReroll.SelectedIndex;
            RecalculateTable();
        }

        private void CboSvReroll_SelectedIndexChanged(object sender, EventArgs e)
        {
            Target.SaveReroll = (Reroll)cboSvReroll.SelectedIndex;
            RecalculateTable();
        }

        private void ChkMoved_Toggled(object sender, ToggledEventArgs e)
        {
            Shooter.Moved = e.Value;
            RecalculateTable();
        }

        private void ChkAdvanced_Toggled(object sender, ToggledEventArgs e)
        {
            Shooter.Advanced = e.Value;
            RecalculateTable();
        }

        private void ChkTermiOrBike_Toggled(object sender, ToggledEventArgs e)
        {
            Shooter.IsTerminatorOrBike = e.Value;
            RecalculateTable();
        }
    }
}
