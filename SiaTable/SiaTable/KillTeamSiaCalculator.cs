﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiaTable
{
    internal class KillTeamSiaCalculator : SiaCalculator
    {
        public override double CalculateExpectedWounds(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            if (!gun.GetsSpecialAmmoKillTeam && ammo != SpecialIssueAmmunition.None)
                return 0.0;

            int numShots = NumShotsVs(shooter, target, gun, ammo);
            int toHit = ToHitVs(shooter, target, gun, ammo);
            int toWound = ToWoundVs(shooter, target, gun, ammo);
            int toSave = ToSaveVs(shooter, target, gun, ammo);
            int postSave = target.FeelNoPainSave;

            // TODO: Kill Team has TONS more modifiers, especially once Specialisms are taken into consideration
            // right now we aren't considering anything beyond the bare minimums

            if (target.IsInLightCover)
            {
                toHit = Math.Min(6, toHit + 1);      // -1 to hit vs models in cover
                toWound = Math.Min(6, toWound + 1);  // -1 to wound vs models in cover
            }

            double halfRange = target.Range / 2;
            if (ammo == SpecialIssueAmmunition.Kraken)
            {
                if (gun.Type == Weapon.WeaponType.Pistol)
                    halfRange += 1.5;
                else
                    halfRange += 3;
            }
            else if (ammo == SpecialIssueAmmunition.Vengeance)
            {
                if (gun.Type == Weapon.WeaponType.Pistol)
                    halfRange -= 1.5;
                else
                    halfRange -= 3;
            }
            if (target.Range > halfRange)
                toHit = Math.Min(6, toHit + 1);

            double oddsToHit = OddsToPassRoll(toHit, shooter.HitReroll);
            double oddsToWound = OddsToPassRoll(toWound, shooter.WoundReroll);
            double oddsToFailSave = 1.0 - OddsToPassRoll(toSave, target.SaveReroll);
            double oddsToFailPostSave = 1.0 - OddsToPassRoll(postSave, Reroll.None);
            double damageMultiplier = DamageMultiplier(shooter, target, gun, ammo);

            return numShots * oddsToHit * oddsToWound * oddsToFailSave * oddsToFailPostSave * damageMultiplier;
        }

        protected override double DamageMultiplier(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            int damageStat = gun.Damage;

            if (gun.KillTeamDamage.HasValue)
                damageStat = gun.KillTeamDamage.Value;

            if (target.Wounds == 1)
                return 1.0;
            else if (damageStat == Weapon.DAMAGE_2D6)
                return 7.0;
            else if (damageStat == Weapon.DAMAGE_D3)
                return 1.5;
            else if (damageStat == Weapon.DAMAGE_D6)
                return 3.5;
            else
                return damageStat;
        }

        protected override int NumShotsVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            int maxRange = gun.Range;
            int shotMultiplier = 1;

            switch (ammo)
            {
                case SpecialIssueAmmunition.Kraken:
                    if (gun.Type == Weapon.WeaponType.Pistol)
                        maxRange = maxRange + 3;
                    else
                        maxRange = maxRange + 6;
                    break;

                case SpecialIssueAmmunition.Vengeance:
                    if (gun.Type == Weapon.WeaponType.Pistol)
                        maxRange = maxRange - 3;
                    else
                        maxRange = maxRange - 6;
                    break;
            }

            switch (gun.Type)
            {
                case Weapon.WeaponType.Pistol:
                    if (target.Range > maxRange)     // pistols can fire while within 1"
                        shotMultiplier = 0;
                    else if (shooter.Advanced)       // cannot fire pistols after advancing
                        shotMultiplier = 0;
                    else
                        shotMultiplier = 1;
                    break;

                case Weapon.WeaponType.RapidFire:
                    if (shooter.Advanced)                                    // cannot fire rapid fire weapons when advancing
                        shotMultiplier = 0;
                    else if (target.Range > maxRange || target.Range <= 1)   // cannot fire in close combat!
                        shotMultiplier = 0;
                    else if (target.Range <= maxRange / 2)                   // double shots within half range!
                        shotMultiplier = 2;
                    else
                        shotMultiplier = 1;
                    break;

                case Weapon.WeaponType.Heavy:
                    if (shooter.Advanced)                                  // cannot fire heavy weapons after advancing
                        shotMultiplier = 0;
                    else if (target.Range > maxRange || target.Range <= 1) // cannot fire in close combat!
                        shotMultiplier = 0;
                    else
                        return gun.NumShots;
                    break;

                default:
                    if (target.Range > maxRange || target.Range <= 1)     // cannot fire in close combat!
                        shotMultiplier = 0;
                    else
                        shotMultiplier = 1;
                    break;
            }

            if (gun.KillTeamNumShots.HasValue)
                return gun.KillTeamNumShots.Value * shotMultiplier;
            else
                return gun.NumShots * shotMultiplier;
        }

        protected override int ToHitVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            int minDice = shooter.BS - gun.ToHitMod; // subtract the modifer from the BS to get the minimum to-hit roll

            if (target.IsInLightCover && ammo == SpecialIssueAmmunition.Dragonfire)
            {
                minDice = minDice - 1;
            }

            // -1 to hit when firing heavies on the move OR assault while advancing
            if ((shooter.Advanced && gun.Type == Weapon.WeaponType.Assault) || (shooter.Moved && gun.Type == Weapon.WeaponType.Heavy))
            {
                minDice = minDice + 1;
            }

            return Math.Min(6, minDice);
        }

        protected override int ToSaveVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            int ap = gun.AP;
            if (gun.KillTeamAP.HasValue)
                ap = gun.KillTeamAP.Value;

            switch (ammo)
            {
                case SpecialIssueAmmunition.Kraken:
                    ap = Math.Max(ap - 1, -2);
                    break;

                case SpecialIssueAmmunition.Vengeance:
                    ap = Math.Max(ap - 2, -3);
                    break;
            }

            if (target.ArmourSave - ap > 6)
                return target.InvulSave;
            else
                return Math.Min(target.InvulSave, target.ArmourSave - ap);
        }

        protected override int ToWoundVs(FiringModel shooter, TargetModel target, Weapon gun, SpecialIssueAmmunition ammo)
        {
            // Hellfire rounds always wound on 2+
            if (!target.IsVehicle && ammo == SpecialIssueAmmunition.Hellfire)
            {
                return 2;
            }
            else
            {
                return ToWoundVs(gun.Strength, target.Toughness);
            }
        }
    }
}
