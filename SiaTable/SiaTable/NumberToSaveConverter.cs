﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace SiaTable
{
    /// <summary>
    /// Converts a number to a text save value
    /// </summary>
    internal class NumberToSaveConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int n = (int)(double)value;

            switch(n)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    return string.Format("{0}+", n);

                default:
                    return "--";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = (string)value;
            s = s.Replace("+", "");

            switch (s)
            {
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                    return int.Parse(s);

                default:
                    return 7;
            }
        }
    }
}
