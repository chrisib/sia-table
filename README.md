Special Issue Ammunition Table
===================================

This is a simple "Mathhammer" app designed to make life easier for Deathwatch players to choose what
Special Issue Ammunition to fire from their bolt weapons in 40k 9th Edition and Kill Team.

Usage
------

Open the app.  You should be greeting by something that looks like this:

![app_screenshot](Doc/siaTable_0.4.0.jpg)

The toggle in the upper-right corner flips between 40k 9e rules and Kill Team.

Use the sliders to set the BS of the firing model, range to the target, and the target's

- Armour Save
- Invulnerable Save
- Feel No Pain Save
- Toughness
- Wounds
- Cover (Dense/Light for 40k, Obscured for Kill Team)

The following reroll modes are also available for the To-Hit roll, To-Wound roll, and Armour Save:

- No reroll
- Reroll 1s
- Reroll all fails
- Reroll all successes

(Note that the current rules may not actually support all of these reroll modes, but they're there anyway)

The table in the lower region will automatically update showing the number of expected wounds caused by
firing each weapon with each type of Special Issue Ammunition.


Limitations
------------

Not every special rule is implemented.  Notably any ability requiring spending Command Points is
not supported.

There is no built-in capacity for to-hit modifiers other than Obscured/Dense Cover.


Notes
------

The `Combi-*` entry represents the Bolter profile of a combi-weapon when fired in addition to its
secondary profile.  As such it inherently includes the flat -1 to hit that firing both weapon
profiles incurs.  The expected wounds does not include the weapon's other profile; just the Bolter
portion.

If the target's Wound characteristic is 1, the expected damage of Damage-2 (or higher) weapons is
treated as 1.  This is because in 40k 9e (and Kill Team) any excess damage on a model is wasted
rather than rolling over onto other models in the unit.



Permissions
------------

This app requires no special permissions; it does not access any internal storage, runs 100% offline,
and doesn't need access to any special device hardware (e.g. camera, GPS, microphone).

This app is ad-free, with no in-app purchases.  If you find it useful, please use it!